# Premier React
Premier projet react, qui techniquement utilise Next.js, mais on s'en sert uniquement comme routeur, on voit surtout les bases de react qui reste valide même sans utiliser Next

## How To Use
1. Cloner le projet
2. Faire un `npm install`
3. Lancer le projet avec `npm run dev`et accéder au résultat sur http://localhost:3000


## Exercices
### Petit compteur
1. Créer une nouvelle variable count en utilisant le useState
2. Afficher cette variable dans le template en dessous du paragraphe par exemple
3. Créer une fonction increment qui va ajouter 1 au count, en utilisant le setCount
4. Rajouter un bouton dans le template qui, au click, va déclencher le increment
5. On fait pareil pour decrement 