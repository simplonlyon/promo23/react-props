import PersonE from '@/entities/PersonE';
import { NextPage } from 'next';
import React from 'react'

const Person:NextPage<PersonE>=(person)=> {
  return (
    <div>{person.lastname} - {person.name}
    <button onClick={person.funcPerson}>Afficher</button>
    </div>
  )
}

export default  Person;