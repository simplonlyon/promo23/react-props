import Props from "@/entities/Props"
import { NextPage } from "next"

const Dogs: NextPage<Props> =(dog)=> {
  return (
    <div>{dog.name} - {dog.breed}</div>
  )
}

export default Dogs
